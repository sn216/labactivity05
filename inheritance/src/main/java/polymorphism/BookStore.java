package polymorphism;

public class BookStore {
    public static void main (String[] args){
        Book[] books = new Book [5];

        books[0] = new Book ("Book 1", "Author 1");
        books[1] = new ElectronicBook ("Ebook1", "Author2", 1234);
        books[2] = new Book ("Book2", "Author3");
        books[3] = new ElectronicBook("Ebook2", "Author4", 5678);
        books[4] = new ElectronicBook("EBook3", "Author5", 9012);

        for (Book book : books){
            System.out.println(book.toString());
        }

        //ElectronicBook b = (ElectronicBook)books[0];
        //System.out.println(b.getNumberBytes());


    }
}
